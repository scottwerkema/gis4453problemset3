#!/usr/bin/python3

import psycopg2
import psycopg2.extras
import os
import sys
import logging


_connection = None

def get_connection():
    global _connection
    if not _connection:
        _connection = psycopg2.connect(dbname='glhydro', user='glhydro', password='SSEdmundFitzg3r@ld', host='scottwerkema.com')
    logging.debug("connection set")
    return _connection

def calculateFlowlineCumulativaPassability():
    cumulativePassabilities = []
    cur = _connection.cursor()
    cur.execute('SELECT DISTINCT(f.rid) FROM flowlines f INNER JOIN barriers b ON f.rid = b.rid') #flowline is a reach # Get only reaches with barriers # 192914 reaches and 52903 have barriers
    flowlines = cur.fetchall()
    i = 0
    for flowline in flowlines:
        cumulativePassability = 1.0
        logging.debug("Flowline: " + str(flowline[0]))
        rowCur = _connection.cursor()
        rowCur.execute('SELECT b.pass10 FROM barriers b WHERE b.rid = %(rid)s', {'rid': flowline[0]})
        barriers = rowCur.fetchall()
        for barrier in barriers:
            cumulativePassability *= float(barrier[0])
        logging.debug("cumulativePassability: " + str(cumulativePassability))
        cumulativePassabilities.append({"rid": int(flowline[0]), "cpass10": cumulativePassability})
        i += 1
        logging.debug("Percent Done: " + str((i/len(flowlines)*100)))
        rowCur.close()
    cur.close()
    logging.debug("Going to UPDATE")
    updateCur = _connection.cursor()
    updateCur.executemany("""UPDATE flowlines SET cpass10 = %(cpass10)s WHERE rid = %(rid)s""", cumulativePassabilities) #this voo doo takes a long time
    _connection.commit()
    updateCur.close()
    logging.debug("Committed")
    
    
def calculateDownstreamFlowlineCumulativaPassabilities(): 
    cur = _connection.cursor()
    cur.execute('SELECT f.rid FROM flowlines f') #flowline is a reach 
    flowlines = cur.fetchall()
    i = 0
    for flowline in flowlines:
        logging.debug("Flowline: " + str(flowline[0]))
        dsCpass10 = calculateDownstreamFlowlineCumulativaPassability(int(flowline[0]))
        logging.debug("dsCpass10: " + str(dsCpass10))
        flowlineUpdateCur = _connection.cursor()
        flowlineUpdateCur.execute("UPDATE flowlines SET cpass10ds = %(dsCpass10)s WHERE rid = %(rid)s", {'dsCpass10': dsCpass10, 'rid': flowline[0]})
        _connection.commit()
        flowlineUpdateCur.close()
        i += 1
        logging.debug("Percent Done: " + str((i/len(flowlines)*100)))
    cur.close()
    
    
def calculateDownstreamFlowlineCumulativaPassability(rid):
    cur = _connection.cursor()
    cur.execute('SELECT f.rid_ds, f.cpass10 FROM flowlines f WHERE f.rid = %(rid)s', {'rid': rid}) #flowline is a reach 
    flowline = cur.fetchall()[0]
    logging.debug("flowline: " + str(rid) + " cpass10: " + str(flowline[1]))
    dsCpass10 = float(flowline[1])
    if (int(flowline[0]) != -1):
        dsCpass10 *= calculateDownstreamFlowlineCumulativaPassability(int(flowline[0]))
    return dsCpass10



def main(argv=None):
    # logging.basicConfig(level=logging.DEBUG)
    logging.basicConfig(filename="cumulativePassability.log",level=logging.DEBUG)
    # logFormatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    
    # rootLogger = logging.getLogger()

    # fileHandler = logging.FileHandler("cumulativePassability.log")
    # fileHandler.setFormatter(logFormatter)
    # rootLogger.addHandler(fileHandler)

    # consoleHandler = logging.StreamHandler()
    # consoleHandler.setFormatter(logFormatter)
    # rootLogger.addHandler(consoleHandler)
    
    logging.debug("In main()")
    #settings, args = process_command_line(argv)
    get_connection()
    # calculateFlowlineCumulativaPassability()
    calculateDownstreamFlowlineCumulativaPassabilities()


    return 0        # success

if __name__ == '__main__':
    status = main()
    sys.exit(status)
    
    
    